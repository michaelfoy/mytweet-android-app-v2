package org.wit.mytweet.model;

import android.support.annotation.NonNull;

import org.wit.android.helpers.DbHelper;

import java.util.List;
import java.util.Random;
import java.util.UUID;

/**
 * @author michaelfoy
 * @version 2016.10.03
 * @file User.java
 * @brief A class to describe a user object and relevant methods
 **/
public class WebToken {

  public Boolean success;
  public String token;
  public User user;

  public WebToken(Boolean success, String token, User user) {
    this.success = success;
    this.token = token;
    this.user = user;
  }
}