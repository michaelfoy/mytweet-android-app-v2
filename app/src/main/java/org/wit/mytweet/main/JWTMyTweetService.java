package org.wit.mytweet.main;


import java.util.List;

import org.wit.mytweet.model.Tweet;
import org.wit.mytweet.model.User;
import org.wit.mytweet.model.WebToken;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface JWTMyTweetService
{
  @GET("/api/users")
  Call<List<User>> getAllUsers();

  @GET("/api/users/{id}")
  Call<User> getUser(@Path("id") String id);

  @POST("/api/users/{id}")
  Call<User> updateUser(@Path("id") String id, @Body User user);

  @POST("/api/users/{id}/tweets")
  Call<String> newTweet(@Path("id") String id, @Body Tweet tweet);

  @GET("/api/users/{id}/tweets")
  Call<List<Tweet>> getTweetsforUser(@Path("id") String id);

  @POST("/api/users/tweet")
  Call<Tweet> getOneTweet(@Body String tweetId);

  @POST("/api/tweets")
  Call<String> deleteTweets(@Body List<String> deletionData);

  @GET("/api/tweets")
  Call<List<Tweet>> getAllTweets();
}
