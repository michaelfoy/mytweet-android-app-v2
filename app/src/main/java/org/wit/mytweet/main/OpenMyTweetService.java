package org.wit.mytweet.main;

import org.wit.mytweet.model.User;
import org.wit.mytweet.model.WebToken;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * @author michaelfoy
 * @version 2017.01.06
 * @file OpenMyTweetService.java
 * @brief Interface to create unauthenticated web server api calls using RetroFit2
 */
public interface OpenMyTweetService {

  /**
   * Web server call to create a new user
   *
   * @param user New user data
   * @return On success return the new user, otherwise null
   */
  @POST("/api/users")
  Call<User> createUser(@Body User user);

  /**
   * Web server call to authenticate a user
   *
   * @param user User data
   * @return On success return the authenticated user with a json web token, otherwise null
   */
  @POST("/api/users/authenticate")
  Call<WebToken> authenticate(@Body User user);
}
