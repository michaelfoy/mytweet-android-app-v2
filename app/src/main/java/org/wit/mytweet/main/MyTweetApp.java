package org.wit.mytweet.main;

import android.app.Application;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.wit.android.helpers.DbHelper;
import org.wit.mytweet.model.Tweet;
import org.wit.mytweet.model.User;
import org.wit.mytweet.model.WebToken;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * @author michaelfoy
 * @version 2016.10.03
 * @file MyTweetApp.java
 * @brief Class to provide back-end application functionality
 */
public class MyTweetApp extends Application {

  public OpenMyTweetService openMyTweetService;
  public JWTMyTweetService jwtMyTweetService;
  public static boolean myTweetServiceAvailable = false;
  public static WebToken webToken = null;

  public static List<User> users = new ArrayList<User>();
  public static List<Tweet> tweets = new ArrayList<Tweet>();
  public static User currentUser;

  public static DbHelper dbHelper = null;
  public static MyTweetApp app;

  // Temporary tweet object, used when creating a new tweet
  private static Tweet tempTweet;

  /**
   * Activates the layout and instantiates it's widgets
   */
  @Override
  public void onCreate() {

    app = this;
    super.onCreate();
    dbHelper = new DbHelper(getApplicationContext());
    dbHelper.onUpgrade(dbHelper.getWritableDatabase(), 1, 1);
    openMyTweetService = RetrofitServiceFactory.createService(OpenMyTweetService.class);

    Log.v("MyTweet", "MyTweet App Started");
  }

  public static MyTweetApp getApp() {
    return app;
  }

  /**
   * Returns the currently logged in user
   *
   * @return The currently logged in user
   */
  public static User getCurrentUser() {
    return currentUser;
  }

  /**
   * Logs out the current user
   */
  public static void logout() {
    webToken = null;
    currentUser = null;
  }

  public static void addTweet(Tweet tweet) {
    dbHelper.addTweet(tweet);
  }

  /**
   * If found, returns a User with the corresponding id
   *
   * @param id User id to be searched in db
   * @return The corresponding User object
   */
  public static User getUserById(String id) {
    users = dbHelper.selectAllUsers();
    for (User user : users) {
      if (user._id.equals(id)) {
        Log.v("MyTweet", "Retrieving: " + user.getFirstName() + " " + user.getLastName());
        return user;
      }
    }
    Log.v("MyTweet", "No user found for id: " + id);
    users = null;
    return null;
  }

  /**
   * If found, returns a Tweet with the corresponding id
   *
   * @param id Tweet id to be searched in db
   * @return The corresponding Tweet object
   */
  public static Tweet getTweetById(String id) {
    tweets = dbHelper.selectAllTweets();
    for (Tweet tweet : tweets) {
      if (tweet._id.equals(id)) {
        Log.v("MyTweet", "Retrieving tweet: " + tweet.getContent());
        return tweet;
      }
    }
    Log.v("MyTweet", "No tweet found for id: " + id);
    tweets = null;
    return null;
  }

  /**
   * Return a list of all tweets composed by a specific user
   *
   * @param id Id of specified user
   * @return List of user's tweets
   */
  public List<Tweet> getAllTweetsForUser(String id) {
    return dbHelper.getAllTweetsForUser(id);
  }

  /**
   * Returns a list of all Tweets in the db
   *
   * @return List of all persisted tweet
   */
  /*public static List<Tweet> getTweets() {
    return dbHelper.selectAllTweets();
  }*/

  /**
   * Deletes a tweet from the db
   *
   * @param id Id of the tweet to be deleted
   */
  public void deleteTweet(String id) {
    dbHelper.deleteTweet(id);
  }

  /**
   * Sets the temporary tweet object before commiting to db
   */
  public static void setTempTweet(Tweet tweet) {
    tempTweet = tweet;
  }

  /**
   * Gets the temporary tweet object before commiting to db
   */
  public static Tweet getTempTweet() {
    return tempTweet;
  }

  /**
   * Deletes the temporary tweet object
   */
  public static void deleteTempTweet() {
    tempTweet = null;
  }

  /**
   * Updates the email address of a user
   *
   * @param email The new email address to be saved
   */
  public void updateEmail(String email) {
    User currentUser = getCurrentUser();
    currentUser.setEmail(email);
    dbHelper.updateUserData("EMAIL", email, currentUser._id);
  }

  /**
   * Updates the password of a user
   *
   * @param password The new password to be saved
   */
  public void updatePassword(String password) {
    User currentUser = getCurrentUser();
    currentUser.setPassword(password);
    dbHelper.updateUserData("PASSWORD", password, currentUser._id);
  }

  public static void setTweets(List<Tweet> tweets) {
    for (int i = 0; i < tweets.size(); i++) {
      dbHelper.populateTweet(tweets.get(i));
    }
  }

  public static void setUsers(List<User> users) {
    for (int i = 0; i < users.size(); i++) {
      dbHelper.populateUser(users.get(i));
    }
  }

  public void serviceAvailableMessage() {
    Toast toast = Toast.makeText(this, "MyTweet Contacted Successfully", Toast.LENGTH_LONG);
    toast.show();
  }

  public void incorrectInputMessage() {
    Toast toast = Toast.makeText(this, "Incorrect Input! :(", Toast.LENGTH_LONG);
    toast.show();
  }

  public void serviceUnavailableMessage() {
    Toast toast = Toast.makeText(this, "MyTweet Service Unavailable. Try again later", Toast.LENGTH_LONG);
    toast.show();
  }

  public void userCreatedMessage() {
    Toast toast = Toast.makeText(this, "New user successfully created", Toast.LENGTH_LONG);
    toast.show();
  }

  public void authenticationErrorMessage() {
    Toast toast = Toast.makeText(this, "Authentication Error: Please log in", Toast.LENGTH_LONG);
    toast.show();
  }
}