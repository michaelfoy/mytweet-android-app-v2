package org.wit.mytweet.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.wit.android.helpers.ContactHelper;
import org.wit.mytweet.model.Tweet;

import org.wit.mytweet.main.MyTweetApp;
import org.wit.mytweet.R;

import static org.wit.android.helpers.IntentHelper.navigateUp;
import static org.wit.android.helpers.IntentHelper.selectContact;
import static org.wit.android.helpers.ContactHelper.sendEmail;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * @file NewTweetFragment.java
 * @brief Controller for NewTweet fragment. Displays either a blank new tweet
 *        or a previously saved tweet with option to email tweet
 * @version 2016.11.01
 * @author michaelfoy
 */
public class NewTweetFragment extends Fragment implements TextWatcher, View.OnClickListener {

  private Button emailButton;
  private Button contactButton;
  private Button tweetButton;
  private EditText tweetText;
  private Tweet tweet;
  private TextView date;
  private TextView chars;
  private int counter;
  private int tweetLength;
  private static final int REQUEST_CONTACT = 1;
  private String emailAddress;
  private String emailName;
  private MyTweetApp app;

  /**
   * Creates the fragment
   *
   * @param savedInstanceState Saved data pertaining to the fragment
   */
  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setHasOptionsMenu(true);
    // Retrieves the currently loaded tweet
    //tweet = MyTweetApp.getTempTweet();
    app = MyTweetApp.getApp();
  }

  /**
   * Activates the layout and instantiates it's widgets
   *
   * @param inflater Inflates the layout view
   * @param parent Viewgroup parent for this fragment
   * @param savedInstanceState Saved data pertaining to the fragment
   * @return View to be displayed
   */
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
    super.onCreateView(inflater, parent, savedInstanceState);
    View v = inflater.inflate(R.layout.newtweet, parent, false);
    NewTweet.actionBar.setDisplayHomeAsUpEnabled(true);
    addListeners(v);
    resetCounter();
    date.setText(editDate());
    return v;
  }

  /**
   * Instantiates widgets and sets their listeners
   *
   * @param v View to be displayed
   */
  private void addListeners(View v) {
    emailButton = (Button) v.findViewById(R.id.emailButton);
    contactButton = (Button) v.findViewById(R.id.contactButton);
    tweetButton = (Button) v.findViewById(R.id.tweetButton);
    tweetText = (EditText) v.findViewById(R.id.tweet);
    chars = (TextView) v.findViewById(R.id.chars);
    date = (TextView) v.findViewById(R.id.date);

    tweetText.addTextChangedListener(this);
    contactButton.setOnClickListener(this);
    emailButton.setOnClickListener(this);
    tweetButton.setOnClickListener(this);
  }

  /**
   * Creates a new Tweet, sends a call to the api to persist tweet in the cloud
   * If successful, tweet persisted remotely & locally, user returned to AllTweets feed.
   * Otherwise, error message displayed, app login data reset, user returned to welcome activity
   */
  public void newTweet() {

    // Checks for an authenticated user and valid web token
    // If not found, user is prompted to log in & returned to log in page
    if ((MyTweetApp.currentUser == null) && !MyTweetApp.myTweetServiceAvailable) {
      MyTweetApp.myTweetServiceAvailable = false;
      MyTweetApp.webToken = null;
      app.authenticationErrorMessage();
      startActivity(new Intent(getActivity(), LogIn.class));

    } else {

      // Web server call to send new tweet to db
      String tweetStr = tweetText.getText().toString();
      String dateStr = date.getText().toString();

      Tweet newTweet = new Tweet(tweetStr, dateStr);
      MyTweetApp.setTempTweet(newTweet);
      Call<String> call = (Call<String>) app.jwtMyTweetService.newTweet(MyTweetApp.getCurrentUser()._id, newTweet);
      call.enqueue(new Callback<String>() {

        // Successful response for call to persist tweet & retrieve tweet._id
        @Override
        public void onResponse(Call<String> call, Response<String> response) {
          MyTweetApp.myTweetServiceAvailable = true;
          Tweet newTweet = MyTweetApp.getTempTweet();
          newTweet._id = response.body();
          MyTweetApp.addTweet(newTweet);
          startActivity(new Intent(getActivity(), AllTweets.class));

        }

        // Unsuccessful response for call to persist tweet & retrieve tweet._id
        @Override
        public void onFailure(Call<String> call, Throwable t) {
          MyTweetApp.myTweetServiceAvailable = false;
          MyTweetApp.webToken = null;
          app.jwtMyTweetService = null;
          startActivity(new Intent(getActivity(), Welcome.class));
          app.serviceUnavailableMessage();
        }
      });
    }
  }

  /**
   * Displays actionbar menu on page
   *
   * @param menu Menu object to be displayed
   * @param inflater The MenuInflater to create the menu
   */
  @Override
  public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
  {
    inflater.inflate(R.menu.mytweet_menu, menu);
    super.onCreateOptionsMenu(menu,inflater);
  }

  /**
   * Returns true if selected menu item is implemented
   *
   * @param item The selected menu item
   * @return True if item implemented
   */
  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case android.R.id.home:
        navigateUp(getActivity());
        return true;

      case R.id.menuItemNewTweet:
        startActivity(new Intent(getActivity(), NewTweet.class));
        return true;

      case R.id.action_mytweets:
        startActivity(new Intent(getActivity(), LoggedInUserTweets.class));
        return true;

      case R.id.action_settings:
        startActivity(new Intent(getActivity(), Settings.class));
        return true;

      case R.id.action_logout:
        MyTweetApp.logout();
        startActivity(new Intent(getActivity(), Welcome.class));
        Toast logoutToast = Toast.makeText(getActivity(), "Successfully logged out :)", Toast.LENGTH_LONG);
        logoutToast.show();
        return true;

      default:
        return super.onOptionsItemSelected(item);
    }
  }

  /**
   * Formats the date input for output
   */
  public String editDate() {
    SimpleDateFormat formatter = new SimpleDateFormat("FF/MM/yyyy - HH:mm");
    Date today = new Date();
    return formatter.format(today);
  }

  /**
   * Retrieves the current length of the tweet message
   *
   * @param charSequence The tweet message
   * @param i            The first character of the sequence
   * @param i1           The length of the message
   * @param i2           Length of new message
   */
  @Override
  public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
    tweetLength = charSequence.length();
  }

  /**
   * Modifies the counter according to the number of allowable characters remaining
   *
   * @param editable The changed tweet message
   */
  @Override
  public void afterTextChanged(Editable editable) {
    if (editable.length() <= 0) {
      resetCounter();
      chars.setText("" + counter);
    } else if (editable.length() > tweetLength) {
      chars.setText("" + (counter -= 1));
    } else if (editable.length() < tweetLength) {
      chars.setText("" + (counter += 1));
    }
  }

  /**
   * Passes data back to app, following a request to another
   *
   * @param requestCode Identifies where the request has returned from
   * @param resultCode The result code returned by child activity
   * @param data Data to be used
   */
  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data) {
    switch (requestCode) {
      case REQUEST_CONTACT:
        emailAddress = ContactHelper.getEmail(getActivity(), data);
        emailName =  ContactHelper.getContact(getActivity(), data);
        contactButton.setText("Send to : " + emailName);
        break;
    }
  }

  /**
   * Implements functionality for the 'tweet', 'contacts' and 'email' buttons
   *
   * @param view The button which has been clicked
   */
  @Override
  public void onClick(View view) {
    switch (view.getId()) {
      case R.id.tweetButton:
        if (tweetText.getText().toString().length() > 0) {
          newTweet();
        } else {
          Toast toast = Toast.makeText(getActivity(), "You forgot the tweet!", Toast.LENGTH_SHORT);
          toast.show();
        }
        break;
      case R.id.contactButton:
        selectContact(getActivity(), REQUEST_CONTACT);
        break;
      case R.id.emailButton:
        sendEmail(getActivity(), emailAddress, "New MyTweet from: " + tweet.getTweeterName(), tweet.getContent());
        break;
    }
  }

  @Override
  public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

  }

  /**
   * Method to reset the tweet text counter
   */
  private void resetCounter() {
    counter = 140;
    chars.setText("" + counter);
  }
}