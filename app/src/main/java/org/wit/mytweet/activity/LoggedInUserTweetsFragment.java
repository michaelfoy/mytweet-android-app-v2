package org.wit.mytweet.activity;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import org.wit.android.helpers.IntentHelper;
import org.wit.mytweet.R;
import org.wit.mytweet.main.MyTweetApp;
import org.wit.mytweet.model.Tweet;

import java.util.List;

/**
 * @file LoggedInUserTweetsFragment.java
 * @brief Controller for LoggedInUserTweets Fragment
 *        Displays a list of the logged-in user's tweets
 * @author michaelfoy
 * @version 2016.11.01
 */
public class LoggedInUserTweetsFragment extends ListFragment implements AdapterView.OnItemClickListener {
  public MyTweetApp app;
  private ListView listView;
  private LoggedInUserAdapter adapter;
  private String CurrentUserId;
  private List<Tweet> tweetList;
  private List<String> deletionData;

  /**
   * Implements the layout, instantiates all fields
   *
   * @param savedInstanceState Data from previously saved instance
   */
  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setHasOptionsMenu(true);
    getActivity().setTitle(R.string.my_tweet_profile);
    app = MyTweetApp.getApp();
    CurrentUserId = MyTweetApp.getCurrentUser()._id;
    tweetList = app.getAllTweetsForUser(CurrentUserId);
    adapter = new LoggedInUserAdapter(getActivity(), tweetList);
    setListAdapter(adapter);
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
    View v = super.onCreateView(inflater, parent, savedInstanceState);
    listView = (ListView) v.findViewById(android.R.id.list);
    return v;
  }

  /**
   * Opens individual tweet view
   *
   * @param l
   * @param v
   * @param position
   * @param id
   */
  @Override
  public void onListItemClick(ListView l, View v, int position, long id) {
    Tweet tweet = ((LoggedInUserAdapter) getListAdapter()).getItem(position);
    IntentHelper.startActivityWithData(getActivity(), TweetDisplay.class, "TWEET_ID", tweet._id);
  }

  /**
   * Listener for click on Tweet item in list. If selected tweet was posted
   * by the logged-in-user, option to email tweet supplied. Otherwise, tweet is read-only.
   *
   * @param parent The parent adapter view
   * @param view The view that was clicked within the adapterView
   * @param position Position of the view in the adapter
   * @param id Row id of clicked item
   */
  @Override
  public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
  }

  /**
   * Displays actionbar menu on page
   *
   * @param menu Menu object to be displayed
   * @param inflater The MenuInflater to create the menu
   */
  @Override
  public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
  {
    inflater.inflate(R.menu.user_profile_menu, menu);
    super.onCreateOptionsMenu(menu,inflater);
  }

  /**
   * Returns true if selected menu item is implemented
   *
   * @param item The selected menu item
   * @return True if item implemented
   */
  @Override
  public boolean onOptionsItemSelected(MenuItem item)
  {
    switch (item.getItemId())
    {
      case R.id.menuItemNewTweet:
        startActivity(new Intent(getActivity(), UserNewTweet.class));
        return true;

      case R.id.action_settings:
        startActivity(new Intent(getActivity(), Settings.class));
        return true;

      case R.id.action_logout:
        MyTweetApp.logout();
        startActivity(new Intent(getActivity(), Welcome.class));
        Toast logoutToast = Toast.makeText(getActivity(), "Successfully logged out :)", Toast.LENGTH_LONG);
        logoutToast.show();
        return true;

      default: return super.onOptionsItemSelected(item);
    }
  }
}

class LoggedInUserAdapter extends ArrayAdapter<Tweet> {
  private Context context;
  public LoggedInUserAdapter(Context context, List<Tweet> tweets) {
    super(context, 0, tweets);
    this.context = context;
  }

  @Override
  public View getView(int position, View convertView, ViewGroup parent) {
    LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    if (convertView == null) {
      convertView = inflater.inflate(R.layout.list_item_tweet, null);
    }
    Tweet tweet = getItem(position);

    TextView tweetContent = (TextView) convertView.findViewById(R.id.tweetListItemContent);
    tweetContent.setText(tweet.getContent());

    TextView dateTextView = (TextView) convertView.findViewById(R.id.tweetListItemDate);
    dateTextView.setText(tweet.getDate());

    return convertView;
  }
}