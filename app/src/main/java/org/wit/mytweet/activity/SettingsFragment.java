package org.wit.mytweet.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import static org.wit.android.helpers.IntentHelper.navigateUp;

import org.wit.mytweet.R;
import org.wit.mytweet.main.MyTweetApp;
import org.wit.mytweet.model.Tweet;
import org.wit.mytweet.model.User;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * @file SettingsFragment.java
 * @brief Provides view and methods for Settings adjustment
 * @author michaelfoy
 * @version 2016.11.06
 */
public class SettingsFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener {

  private SharedPreferences prefs;
  private MyTweetApp app;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setHasOptionsMenu(true);
    app = MyTweetApp.getApp();
    addPreferencesFromResource(R.xml.settings);
  }

  @Override
  public void onStart()
  {
    super.onStart();
    prefs = PreferenceManager
        .getDefaultSharedPreferences(getActivity());
    prefs.registerOnSharedPreferenceChangeListener(this);
  }

  /**
   * Directs app to action when menu item selected
   *
   * @param item The selected item
   * @return True if action successful
   */
  @Override
  public boolean onOptionsItemSelected(MenuItem item)
  {
    switch (item.getItemId())
    {
      case android.R.id.home:
        navigateUp(getActivity());
        return true;

      case R.id.action_mytweets:
        startActivity(new Intent(getActivity(), LoggedInUserTweets.class));
        return true;

      case R.id.action_settings:
        startActivity(new Intent(getActivity(), Settings.class));
        return true;

      case R.id.action_logout:
        MyTweetApp.logout();
        startActivity(new Intent(getActivity(), Welcome.class));
        Toast logoutToast = Toast.makeText(getActivity(), "Successfully logged out :)", Toast.LENGTH_LONG);
        logoutToast.show();
        return true;
      default:
        return super.onOptionsItemSelected(item);
    }
  }

  /**
   * Directs the app upon shred preference update
   *
   * @param sharedPreferences Object containing modified data
   * @param key Key identifying which setting has been modified
   */
  @Override
  public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
    String value = sharedPreferences.getString(key, "null");
    User userEdit = new User("", "", "", "");
    if (value.equals("null")) {
      Log.v("MyTweet", "No value entered for setting");

      // Update email address
    } else if (key.equals("email")) {
      userEdit.setEmail(value);
      updateUserDetails(userEdit);
      app.updateEmail(value);

      // Update password
    } else if (key.equals("password")) {
      userEdit.setPassword(value);
      updateUserDetails(userEdit);
      app.updatePassword(value);

    } else {
      Log.v("MyTweet", "Error handling shared preference, key not identified");
    }
  }

  private void updateUserDetails(User userEdit) {

    // Checks for an authenticated user and valid web token
    // If not found, user is prompted to log in & returned to log in page
    if ((MyTweetApp.currentUser == null) && !MyTweetApp.myTweetServiceAvailable) {
      MyTweetApp.myTweetServiceAvailable = false;
      MyTweetApp.webToken = null;
      app.authenticationErrorMessage();
      startActivity(new Intent(getActivity(), LogIn.class));

    } else {

      // Web server call to send new user data to db
      Call<User> call = (Call<User>) app.jwtMyTweetService.updateUser(MyTweetApp.getCurrentUser()._id, userEdit);
      call.enqueue(new Callback<User>() {

        // Successful response for call to modify user data
        @Override
        public void onResponse(Call<User> call, Response<User> response) {
          MyTweetApp.myTweetServiceAvailable = true;
          app.updateEmail(response.body().email);
          app.updatePassword(response.body().password);
          startActivity(new Intent(getActivity(), AllTweets.class));

        }

        // Unsuccessful response for call to modify user data
        @Override
        public void onFailure(Call<User> call, Throwable t) {
          MyTweetApp.myTweetServiceAvailable = false;
          MyTweetApp.webToken = null;
          app.jwtMyTweetService = null;
          startActivity(new Intent(getActivity(), Welcome.class));
          app.serviceUnavailableMessage();
        }
      });
    }
  }

  @Override
  public void onStop()
  {
    super.onStop();
    prefs.unregisterOnSharedPreferenceChangeListener(this);
  }
}