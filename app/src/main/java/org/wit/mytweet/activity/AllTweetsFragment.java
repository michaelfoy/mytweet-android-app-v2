package org.wit.mytweet.activity;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import org.wit.mytweet.R;
import org.wit.mytweet.main.MyTweetApp;
import org.wit.mytweet.model.Tweet;
import org.wit.android.helpers.IntentHelper;
import org.wit.mytweet.model.User;

import java.util.List;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * @author michaelfoy
 * @version 2016.10.18
 * @file AllTweetsFragment.java
 * @brief Controller for TweetListFragment activity,
 * Displays a global list of all tweets
 */
public class AllTweetsFragment extends ListFragment implements AdapterView.OnItemClickListener {
  public MyTweetApp app;

  /**
   * Implements the layout, instantiates all fields
   *
   * @param savedInstanceState Data from previously saved instance
   */
  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setHasOptionsMenu(true);
    getActivity().setTitle(R.string.my_tweet_feed);
    app = MyTweetApp.getApp();
  }

  /**
   * Calls the api to get all users & get all tweets.
   * Checks for a logged in user to process api call,
   * if the call fails, the app is reset and user returned to welcome page
   */
  @Override
  public void onResume()
  {
    super.onResume();

    // Checks for an authenticated user and valid web token
    // If not found, user is prompted to log in & returned to log in page
    if ((MyTweetApp.currentUser == null) && !MyTweetApp.myTweetServiceAvailable) {
      MyTweetApp.myTweetServiceAvailable = false;
      MyTweetApp.webToken = null;
      app.authenticationErrorMessage();
      startActivity(new Intent(getActivity(), LogIn.class));

    } else {

      // Web server call to retrieve users from db
      Call<List<User>> call1 = (Call<List<User>>) app.jwtMyTweetService.getAllUsers();
      call1.enqueue(new Callback<List<User>>() {

        // Successful response for call to retrieve users
        @Override
        public void onResponse(Call<List<User>> call, Response<List<User>> response) {
          MyTweetApp.myTweetServiceAvailable = true;
          MyTweetApp.setUsers(response.body());
          app.serviceAvailableMessage();

        }

        // Unsuccessful response for call to retrieve users
        @Override
        public void onFailure(Call<List<User>> call, Throwable t) {
          MyTweetApp.myTweetServiceAvailable = false;
          MyTweetApp.webToken = null;
          app.jwtMyTweetService = null;
          startActivity(new Intent(getActivity(), Welcome.class));
          app.serviceUnavailableMessage();
        }
      });

      // Web server call to retrieve tweets from db
      Call<List<Tweet>> call2 = (Call<List<Tweet>>) app.jwtMyTweetService.getAllTweets();
      call2.enqueue(new Callback<List<Tweet>>() {

        // Successful response for call to retrieve tweets
        @Override
        public void onResponse(Call<List<Tweet>> call, Response<List<Tweet>> response) {
          MyTweetApp.myTweetServiceAvailable = true;
          List<Tweet> allTweets = response.body();
          /*for (int i = 0; i < allTweets.size(); i++) {
            allTweets.get(i).tweeter = allTweets.get(i).tweeter._id;
          }*/
          MyTweetApp.setTweets(response.body());
          List<Tweet> tweetList = response.body();
          AllTweetsAdapter adapter = new AllTweetsAdapter(getActivity(), tweetList);
          setListAdapter(adapter);
          app.serviceAvailableMessage();
        }

        // Unsuccessful response for call to retrieve tweets
        @Override
        public void onFailure(Call<List<Tweet>> call, Throwable t) {
          MyTweetApp.myTweetServiceAvailable = false;
          MyTweetApp.webToken = null;
          app.jwtMyTweetService = null;
          startActivity(new Intent(getActivity(), Welcome.class));
          app.serviceUnavailableMessage();
        }
      });
    }
  }

  /**
   * Opens individual tweet view
   * If tweet belongs to logged in user, opened view has email functionality
   * Else, tweet view is read-only
   *
   * @param l
   * @param v
   * @param position
   * @param id
   */
  @Override
  public void onListItemClick(ListView l, View v, int position, long id) {
    Tweet tweet = ((AllTweetsAdapter) getListAdapter()).getItem(position);
    IntentHelper.startActivityWithData(getActivity(), TweetDisplay.class, "TWEET_ID", tweet._id);
  }

  /**
   * Listener for click on Tweet item in list. If selected tweet was posted
   * by the logged-in-user, option to email tweet supplied. Otherwise, tweet is read-only.
   *
   * @param parent   The parent adapter view
   * @param view     The view that was clicked within the adapterView
   * @param position Position of the view in the adapter
   * @param id       Row id of clicked item
   */
  @Override
  public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
  }

  /**
   * Displays actionbar menu on page
   *
   * @param menu     Menu object to be displayed
   * @param inflater The MenuInflater to create the menu
   */
  @Override
  public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
    inflater.inflate(R.menu.mytweet_menu, menu);
    super.onCreateOptionsMenu(menu, inflater);
  }

  /**
   * Returns true if selected menu item is implemented
   *
   * @param item The selected menu item
   * @return True if item implemented
   */
  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case R.id.menuItemNewTweet:
        startActivity(new Intent(getActivity(), NewTweet.class));
        return true;

      case R.id.action_mytweets:
        startActivity(new Intent(getActivity(), LoggedInUserTweets.class));
        return true;

      case R.id.action_settings:
        startActivity(new Intent(getActivity(), Settings.class));
        return true;

      case R.id.action_logout:
        MyTweetApp.logout();
        startActivity(new Intent(getActivity(), Welcome.class));
        Toast logoutToast = Toast.makeText(getActivity(), "Successfully logged out :)", Toast.LENGTH_LONG);
        logoutToast.show();
        return true;

      default:
        return super.onOptionsItemSelected(item);
    }
  }
}

class AllTweetsAdapter extends ArrayAdapter<Tweet> {
  private Context context;

  public AllTweetsAdapter(Context context, List<Tweet> tweets) {
    super(context, 0, tweets);
    this.context = context;
  }

  @Override
  public View getView(int position, View convertView, ViewGroup parent) {
    LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    if (convertView == null) {
      convertView = inflater.inflate(R.layout.list_item_tweet, null);
    }
    Tweet tweet = getItem(position);

    TextView tweetContent = (TextView) convertView.findViewById(R.id.tweetListItemContent);
    tweetContent.setText(tweet.getContent());

    TextView dateTextView = (TextView) convertView.findViewById(R.id.tweetListItemDate);
    dateTextView.setText(tweet.getDate());

    TextView tweeter = (TextView) convertView.findViewById(R.id.tweetListItemTweeter);
    tweeter.setText(tweet.getTweeterName());

    return convertView;
  }
}