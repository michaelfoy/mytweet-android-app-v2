package org.wit.mytweet.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import org.wit.mytweet.main.MyTweetApp;
import org.wit.mytweet.R;
import org.wit.mytweet.model.User;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * @author michaelfoy
 * @version 2016.10.03
 * @file SignUp.java
 * @brief Class to provide functionality to signup.xml layout
 */
public class SignUp extends AppCompatActivity implements View.OnClickListener, Callback<User> {

  private Button registerButton;
  private EditText firstName;
  private EditText lastName;
  private EditText email;
  private EditText password;
  private MyTweetApp app;

  /**
   * Activates the layout and instantiates it's widgets
   *
   * @param savedInstanceState Saved data pertaining to the activity
   */
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.signup);

    firstName = (EditText) findViewById(R.id.firstName);
    lastName = (EditText) findViewById(R.id.lastName);
    email = (EditText) findViewById(R.id.email);
    password = (EditText) findViewById(R.id.password);
    registerButton = (Button) findViewById(R.id.register);
    registerButton.setOnClickListener(this);
    app = (MyTweetApp) getApplication();
  }

  /**
   * Implements the onClickListener for the sign up button
   *
   * @param v Login button view
   */
  @Override
  public void onClick(View v) {
    switch (v.getId()) {
      case R.id.register:
        buttonPressed(v);
        break;
    }
  }

  /**
   * Registers a new user with the web server
   *
   * @param view SignUp view
   */
  public void buttonPressed(View view) {

    String firstNameStr = firstName.getText().toString();
    String lastNameStr = lastName.getText().toString();
    String emailStr = email.getText().toString();
    String passwordStr = password.getText().toString();

    User newUser = new User(firstNameStr, lastNameStr, emailStr, passwordStr);
    Call<User> call = (Call<User>) app.openMyTweetService.createUser(newUser);
    call.enqueue(this);
  }

  /**
   * On successful web server call, checks response.
   * If response empty, redirects to Welcome page with error message
   * Otherwise redirects to LogIn page with success message
   *
   * @param call     Asynchronous call to web server
   * @param response Response from the web server
   */
  @Override
  public void onResponse(Call<User> call, Response<User> response) {
    if (response.body() == null) {
      app.incorrectInputMessage();
      startActivity(new Intent(this, Welcome.class));
    } else {
      app.userCreatedMessage();
      startActivity(new Intent(this, LogIn.class));
      Log.v("MyTweet", "Log in successful");
    }
  }

  /**
   * On unsuccessful web server call, displays error message
   *
   * @param call Asynchronous web server call
   * @param t    Exception
   */
  @Override
  public void onFailure(Call<User> call, Throwable t) {
    MyTweetApp.myTweetServiceAvailable = false;
    app.serviceUnavailableMessage();
  }

  @Override
  public void onStart() {
    super.onStart();
  }

  @Override
  public void onStop() {
    super.onStop();
  }
}