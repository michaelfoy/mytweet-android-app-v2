package org.wit.mytweet.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import org.wit.mytweet.R;
import org.wit.mytweet.main.JWTMyTweetService;
import org.wit.mytweet.main.MyTweetApp;
import org.wit.mytweet.main.RetrofitServiceFactory;
import org.wit.mytweet.model.User;
import org.wit.mytweet.model.WebToken;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * @author michaelfoy
 * @version 2016.09.25
 * @file Login.java
 * @brief Class to provide functionality to activity_login.xml layout
 */
public class LogIn extends AppCompatActivity implements View.OnClickListener, Callback<WebToken> {

  private Button loginButton;
  private EditText email;
  private EditText password;
  private MyTweetApp app;

  /**
   * Activates the layout and instantiates it's widgets
   *
   * @param savedInstanceState Saved data pertaining to the activity
   */
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.login);

    loginButton = (Button) findViewById(R.id.loginButton);
    loginButton.setOnClickListener(this);
    email = (EditText) findViewById(R.id.email);
    password = (EditText) findViewById(R.id.password);
    app = (MyTweetApp) getApplication();
  }

  /**
   * Implements the onClickListener for the login button
   *
   * @param v Login button view
   */
  @Override
  public void onClick(View v) {
    switch (v.getId()) {
      case R.id.loginButton:
        buttonPressed();
        break;
    }
  }

  /**
   * If user data is correct, logs in the user using a call to api
   */
  public void buttonPressed() {

    EditText email = (EditText) findViewById(R.id.email);
    EditText password = (EditText) findViewById(R.id.password);

    String emailStr = email.getText().toString();
    String passwordStr = password.getText().toString();

    User user = new User("", "", emailStr, passwordStr);
    Call<WebToken> call = (Call<WebToken>) app.openMyTweetService.authenticate(user);
    call.enqueue(this);
  }

  /**
   * Following a successful api call, checks the response.
   * In the case that the response is empty, redirects to Welcome page
   * Otherwise, updates app data for logged-in user and opens homepage
   *
   * @param call     Asynchronous call to web server api
   * @param response Response from web server
   */
  @Override
  public void onResponse(Call<WebToken> call, Response<WebToken> response) {
    if (response.body() == null) {
      app.incorrectInputMessage();
      startActivity(new Intent(this, Welcome.class));
    } else {
      app.serviceAvailableMessage();

      // Applies authentication data to MyTweetApp fields
      MyTweetApp.webToken = response.body();
      MyTweetApp.currentUser = MyTweetApp.webToken.user;
      MyTweetApp.webToken.user = null;
      MyTweetApp.myTweetServiceAvailable = true;

      // Creates authenticated MyTweetService with json web token
      app.jwtMyTweetService = RetrofitServiceFactory.createService(JWTMyTweetService.class, MyTweetApp.webToken.token);

      startActivity(new Intent(this, AllTweets.class));
      Log.v("MyTweet", "Log in successful");
    }
  }

  /**
   * If web server call unsuccessful, displays error message
   * and ensures app.myTweetServiceAvailable is set to false
   *
   * @param call Asynchronous call to web server api
   * @param t    Exception
   */
  @Override
  public void onFailure(Call<WebToken> call, Throwable t) {
    MyTweetApp.myTweetServiceAvailable = false;
    app.serviceUnavailableMessage();
  }

  @Override
  public void onStart() {
    super.onStart();
  }

  @Override
  public void onStop() {
    super.onStop();
  }
}
