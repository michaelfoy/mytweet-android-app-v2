package org.wit.mytweet.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


import org.wit.mytweet.main.MyTweetApp;
import org.wit.mytweet.R;
import org.wit.mytweet.model.Tweet;

import static org.wit.android.helpers.ContactHelper.sendEmail;
import static org.wit.android.helpers.IntentHelper.navigateUp;
import static org.wit.android.helpers.IntentHelper.selectContact;

/**
 * @file TweetDisplayFragment.java
 * @brief Controller for TweetDisplay fragment
 * @version 2016.11.01
 * @author michaelfoy
 */
public class TweetDisplayFragment extends Fragment implements View.OnClickListener {

  private Tweet tweet;
  private static final int REQUEST_CONTACT = 1;
  private String emailAddress;
  private MyTweetApp app;

  /**
   * Creates the fragment
   *
   * @param savedInstanceState Saved data pertaining to the fragment
   */
  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setHasOptionsMenu(true);
    String tweetId = (String) getActivity().getIntent().getExtras().getSerializable("TWEET_ID");
    app = MyTweetApp.getApp();
    tweet = MyTweetApp.getTweetById(tweetId);
    Log.v("MyTweet","Display tweet page opened");
  }

  /**
   * Activates the layout and instantiates it's widgets
   *
   * @param inflater Inflates the layout view
   * @param parent Viewgroup parent for this fragment
   * @param savedInstanceState Saved data pertaining to the fragment
   * @return View to be displayed
   */
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
    super.onCreateView(inflater, parent, savedInstanceState);
    View v = inflater.inflate(R.layout.tweet_display, parent, false);
    //NewTweet newTweet = (NewTweet)getActivity();
    TweetDisplay.actionBar.setDisplayHomeAsUpEnabled(true);
    initializeWidgets(v);
    return v;
  }

  /**
   * Initializes the widgets
   *
   * @param v The view where widgets are displayed
   */
  private void initializeWidgets(View v) {

    TextView tweeter;
    TextView date;
    TextView tweetText;
    Button contactButton;
    Button emailButton;


    tweetText = (TextView) v.findViewById(R.id.tweet);
    tweeter = (TextView) v.findViewById(R.id.tweeter);
    date = (TextView) v.findViewById(R.id.date);
    emailButton = (Button) v.findViewById(R.id.emailButton);
    contactButton = (Button) v.findViewById(R.id.contactButton);

    tweetText.setText(tweet.getContent());
    tweeter.setText(tweet.getTweeterName());
    date.setText(tweet.getDate());

    emailButton.setOnClickListener(this);
    contactButton.setOnClickListener(this);
  }

  /**
   * Implements functionality for the 'tweet', 'contacts' and 'email' buttons
   *
   * @param view The button which has been clicked
   */
  @Override
  public void onClick(View view) {
    switch (view.getId()) {
      case R.id.contactButton:
        selectContact(getActivity(), REQUEST_CONTACT);

        break;
      case R.id.emailButton:
        sendEmail(getActivity(), emailAddress, "New MyTweet from: " + tweet.getTweeterName(), tweet.getContent());
        break;
    }
  }

  /**
   * Displays actionbar menu on page
   *
   * @param menu Menu object to be displayed
   * @param inflater The MenuInflater to create the menu
   */
  @Override
  public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
  {
    inflater.inflate(R.menu.mytweet_menu, menu);
    super.onCreateOptionsMenu(menu,inflater);
  }

  /**
   * Returns true if selected menu item is implemented
   *
   * @param item The selected menu item
   * @return True if item implemented
   */
  @Override
  public boolean onOptionsItemSelected(MenuItem item)
  {
    switch (item.getItemId())
    {
      case android.R.id.home:  navigateUp(getActivity());
        return true;

      case R.id.menuItemNewTweet:
        startActivity(new Intent(getActivity(), NewTweetFragment.class));
        return true;

      case R.id.action_mytweets:
        startActivity(new Intent(getActivity(), LoggedInUserTweets.class));
        return true;

      case R.id.action_settings:
        startActivity(new Intent(getActivity(), Settings.class));
        return true;

      case R.id.action_logout:
        MyTweetApp.logout();
        startActivity(new Intent(getActivity(), Welcome.class));
        Toast logoutToast = Toast.makeText(getActivity(), "Successfully logged out :)", Toast.LENGTH_LONG);
        logoutToast.show();
        return true;

      default: return super.onOptionsItemSelected(item);
    }
  }

}
