# ICTSkills Mobile Application Development (2017)
# Assignment 2: MyTweet

##### Student name: Michael Foy
##### Student Number: 06695779
##### Date: 09/01/2017

## Description of Application:
This assignment comprises a Twitter-style application for users to post publicly viewable messages. Once a user has registered their details by creating an account they can do the following:
- View the public feed of all messages from all users
- View a feed of one�s own messages
- View individual messages
- Post messages to the public feed
- Email messages
- Adjust their email and password settings
- Logout

### Log in details:
User emails: homer@simpson.com, marge@simpson.com, gary@edams.com, bart@simpson.com
All users have the same password: __bigsecret__

## Repository:
- [BitBucket](https://bitbucket.org/michaelfoy/mytweet-android-app-v2)
